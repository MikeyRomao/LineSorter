package code_mromao_Assignment2;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JButton;
import javax.swing.JPanel;

public class Screen4 extends JPanel
{
	static Integer htarray[];
	static Integer temparray[];
	JButton j,x,l;
	static Graphics tempgraphicsarray[],graphicsarray[];
	static Integer yarray[];
	static Integer tempyarray[];
	Screen2 m;
	
	static int index=0;
	
	//Constructor for Screen4
	public Screen4()
	{
		//Create a new button Reset Lines -> Pushes lines back to how they were before the sort
		BorderLayout border=new BorderLayout();
		setLayout(border);
		j=new JButton("Reset Lines");
		add(j,BorderLayout.EAST);
		j.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) //If reset lines is clicked will take you to screen 2 panel and will let you choose the other sorting method
			{
				m=new Screen2();
				add(m);
				remove(j);
				revalidate();
			}
		});	
		
		
		htarray=new Integer [256]; //Initialize the new htarray (stores the heights of the lines)
		tempgraphicsarray=new Graphics[256]; //Initialize the new tempgraphicsarray (temporary color array for merge sort)
		graphicsarray=new Graphics[256]; //Initialize the new graphicsarray (graphics color array to swap colors when swapping lines)
		yarray=new Integer[256]; //Initialize the yarray (keeps track of the y positions of each line and is modified in paint component so all lines start at y=800
		temparray=new Integer[256]; //Initialize the temporary array(temporary array for merge sort for the temporary height of the lines)
		
		for(int i=0;i<256;i++)
		{
			htarray[i]=new Integer (Screen2.heightarray2[i]); //So that the heights of the lines stay consistent throughout
			graphicsarray[i]=Screen2.array2[i]; //Don't want to modify array2 in any way (so I can reuse it)
			yarray[i]=new Integer(Screen2.yarray2[i]); //So that the y values of the heights stay consistent throughout
			
		}
		//Timer to run MergeIndex
		long period=(long)0.001;
        long time=30;
        final Timer delayTime=new Timer();
        delayTime.scheduleAtFixedRate(new TimerTask(){
        @Override
        public void run()
        {
        	MergeSort(index,255); //Call merge sort with the height array and temporary array and colour arrays
        	delayTime.cancel(); 
        
        }
        },period,time);
       
       
	
        		
 		
	}
	
	protected void paintComponent(Graphics g)
	{
		int x=150;
		for(int i=0;i<256;i++)   //for loop goes through all the lines and sees if how far off they are from the line y=800 and readjusts them to sit on the line
		{
			if(yarray[i]+htarray[i]>800) // Greater than 800, means it's not all on the screen (execute to raise the line)
			{
				int l=(yarray[i]+htarray[i])-800;
				yarray[i]-=l;
			}
			else if((yarray[i]+htarray[i])==800) 
			{
				//Do nothing
			}
			else // Less than 800, have to push the line down
			{
				int m= 800-(yarray[i]+htarray[i]);
				yarray[i]+=m;
			}
				
			g.setColor(graphicsarray[i].getColor()); //sets the color to the temporary graphics array color to match the colors from selection sort or from line scramble
			g.fillRect(x, yarray[i], 1, htarray[i]);
			x+=2;
		}
	}
	
	public <T extends Comparable<? super T>> void MergeSort(int lowindex, int highindex)
	{
		//Recursively incorporating the MergeSort 
		if(lowindex<highindex)
		{
			int middle= (lowindex+ highindex)/2;
			MergeSort(lowindex,middle); //Separate the left side
			MergeSort(middle+1,highindex); //Separate the right side
			MergeSides(lowindex,middle+1,highindex);//Merge sides together
			pause();
			revalidate();
			repaint();
		}
	}
	
	public <T extends Comparable<? super T>> void MergeSides(int lowindex, int highindex,int highend)
	{
		int lowend= highindex-1;
		int x= lowindex;
		int y= highend-lowindex+1;
		
		while(lowindex<=lowend &&highindex<=highend)
		{
			if(htarray[lowindex].compareTo(htarray[highindex])<=0)
			{
				temparray[x++]=htarray[lowindex++];
				swapColors(lowindex-1,x-1);
			}
			else
			{
				temparray[x++]=htarray[highindex++];
				swapColors(highindex-1,x-1);
			}
		}
		
		while(lowindex<=lowend)
		{
			temparray[x++]=htarray[lowindex++];
			swapColors(lowindex-1,x-1);
		}
		
		while(highindex<=highend)
		{
			temparray[x++]=htarray[highindex++];
			swapColors(highindex-1,x-1);
		}
		
		//Copying temp array back to the original array
		for(int i=0;i<y;i++,highend--)
		{
			htarray[highend]=temparray[highend];
			swapAllColors(highend);
			pause();
			revalidate();
			repaint();
		}
				
	}
	
	public static void swapColors(int y, int z) //Wouldn't let me swap the colors inside the actual function, so created a new function
	{
		tempgraphicsarray[z++]=graphicsarray[y++];		
	}
	
	public static void swapAllColors(int y) //Do the last for loop to copy the temporary color array to the actual color array to be displayed
	{
		graphicsarray[y]=tempgraphicsarray[y];
	}
	
	void pause()
	{
		try {
			   // thread to sleep for 10 milliseconds
			   Thread.sleep(5);
			   } catch (Exception e) {
			   System.out.println(e);
			   }
	}
	
}
