package code_mromao_Assignment2;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.util.*;

public class Screen3 extends JPanel
{
	static Integer[] heightarray,tempyarray;
	static Graphics[] temparray;
	JButton j;
	Screen2 m;
	static int index=0;
	
	//Constructor for Screen3
	public Screen3()
	{
		//Create new button reset lines-> resets lines to how the were before the sort
		BorderLayout border=new BorderLayout();
		setLayout(border);
		j=new JButton("Reset Lines");
		add(j,BorderLayout.EAST);
		j.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) //Clicking reset lines, brings you back to screen2
			{
				m=new Screen2();
				add(m);
				remove(j);
				revalidate();
			}
		});	
		
		heightarray=new Integer [256]; //initialize height array
		tempyarray=new Integer[256];
		temparray=new Graphics[256];
		for(int i=0;i<256;i++)
		{
			heightarray[i]=new Integer (Screen2.heightarray2[i]); //set the array equal to the height array in Screen2 class for consistency
			tempyarray[i]=new Integer (Screen2.yarray2[i]);
			temparray[i]=(Screen2.array2[i]);
		}
		
		
		//Implementing the timer and creating the effect of the lines
        long period=(long)0.001;
        long time=50;
        final Timer delayTime=new Timer();
        delayTime.scheduleAtFixedRate(new TimerTask(){
        @Override
        public void run()
        {
        	selectionSort(index);//call selection sort and pass the current index (0 at present time)
        	if(++index==256) //if the index is 256, stop the timer
        	{
        		delayTime.cancel(); 
        	}
        }
        },period,time);
        
		index=0;
       
	}

	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int x=150;
		
		//Draw the lines on the screen
		for(int i=0;i<256;i++)
		{
			g.setColor(temparray[i].getColor());
			g.fillRect(x, tempyarray[i], 1, heightarray[i]);
			x+=2;
			
		}	
	}
	
	public <T extends Comparable<? super T>> void selectionSort(int index) { //Implements selection sort using iteration
		int indexofNextSmallest=getIndexOfSmallest(index,255);
		swap(index, indexofNextSmallest);
		revalidate();
		repaint();
		}
	
	public static <T extends Comparable<? super T>> int getIndexOfSmallest(int first,int last) //Gets IndexofSmallest and returns it to be swapped
	{
		int min=heightarray[first];
		int indexOfMin=first;
		for(int index=first+1;index<=last;index++)
		{
			if(heightarray[index].compareTo(min)<0)
			{
				min=heightarray[index];
				indexOfMin=index;
			}
		}
		return indexOfMin;
	}
	
	public static void swap(int i,int j) //Swaps the y-values, heights and colors of the lines
	{
		int temp=heightarray[i];
		heightarray[i]=heightarray[j];
		heightarray[j]=temp;
		
		//Swap the y-values of the heights also
		int b=tempyarray[i];
		tempyarray[i]=tempyarray[j];
		tempyarray[j]=b;
		
		//Swap the colors of the lines also
		Graphics c=temparray[i];
		temparray[i]=temparray[j];
		temparray[j]=c;
	}	
	
}
