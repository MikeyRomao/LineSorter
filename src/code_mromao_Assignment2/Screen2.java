package code_mromao_Assignment2;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Screen2 extends JPanel 
{
	static Graphics array2[],temparray[];
	static int heightarray2[],yarray2[],tempheightarray[],tempyarray[];
	static int count=0,mergesort=0,selectionsort=0;
	JButton x,l,z;
	Screen2 a;
	Screen3 m;
	Screen4 f;
	
	public Screen2()
	{	
		array2=new Graphics[256]; //initializing array2
		heightarray2=new int [256]; //initializing heightarray2
		yarray2=new int[256]; //initializing yarray2
		
		temparray=new Graphics[256];
		tempheightarray=new int[256];
		tempyarray=new int[256];
		
		
		for(int i=0;i<256;i++) //Copying the elements of Screen.array1 into array2, so Screen.array1 can stay in tact, and y also
		{
			array2[i]=Screen.array[i];
			heightarray2[i]=Screen.heightarray[i];
			yarray2[i]=Screen.yarray[i];	
		}
		
		
		
	if(count%2==0) //count increases twice after every time selection sort and merge sort have each been finished (to re-enable choosing both and scrambling the lines)
	{
		
		BorderLayout border=new BorderLayout();
		setLayout(border);
		x=new JButton("Selection Sort");
		add(x,BorderLayout.EAST);
		x.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) //On SelectionSort click
			{
				m=new Screen3();
				add(m);
				remove(l);
				remove(x);
				remove(z);
				revalidate();
				selectionsort++;
				count++;
			}
		});	
		//Creating a new button called merge sort
		l=new JButton("Merge Sort");
		add(l,BorderLayout.WEST);
		l.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) //On MergeSort click
			{
				f=new Screen4();
				add(f);
				remove(l);
				remove(x);
				remove(z);
				revalidate();
				mergesort++;
				count++;
			}
		});
		//Creating a new button "Scramble Lines"
		z=new JButton("Scramble Lines");
		add(z,BorderLayout.NORTH);
		z.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) //On Scramble Lines click 
			{
				a=new Screen2(); //essentially does the paint component in this screen and can continue doing so until you click another button
				add(a);
				remove(z);
				remove(l);
				remove(x);
				revalidate();	
			}
		});
		
	}
	
	if(count%2!=0&&mergesort>selectionsort) //In this case, since merge sort was chosen at first, make it so that you can't scramble lines or choose selection sort
	{
		BorderLayout border=new BorderLayout();
		setLayout(border);
		x=new JButton("Selection Sort");
		add(x,BorderLayout.EAST);
		x.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) //On button click, will take you to selection sort screen to sort the lines
			{
				m=new Screen3();
				add(m);
				remove(x);
				revalidate();
				repaint();
				selectionsort++;
				count++;
			}
		});	
	}
	
	else if(count%2!=0&&selectionsort>mergesort) // In this case, since selection sort was chosen at first
	{
		BorderLayout border= new BorderLayout();
		setLayout(border);
		l=new JButton("Merge Sort");
		add(l,BorderLayout.WEST);
		l.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) // On button click, will take you to merge sort screen to sort the lines
			{
				f=new Screen4();
				add(f);
				remove(l);
				revalidate();
				mergesort++;
				count++;
			}
		});
		
	}
	
	}
	protected void paintComponent(Graphics g)
	{
		
		super.paintComponent(g);
		int x=650,size=256;
		
		randomize();
		
		for(int i=0;i<size;i++) //Displaying the scrambled lines on the screen
		{
			
			g.setColor(array2[i].getColor());
			g.fillRect(x,yarray2[i], 1, heightarray2[i]);
			x-=2;
			
		}	
}
	
	static void randomize()
	{
		Random rnd = ThreadLocalRandom.current(); //Implementing the Fisher-Yates Shuffle, making sure heights/colours/y values are passed in scrambling
		for (int i=255; i>0; i--)
		{
			int index = rnd.nextInt(i + 1);
			
			/* Below set of assignments essentially swaps the y positions, the heights and the colors of the lines
			 * 
			 */
			Graphics temp = array2[index]; 
			int temp2= heightarray2[index];
			int temp3= yarray2[index];
			
			array2[index] = array2[i];
			heightarray2[index]=heightarray2[i];
			yarray2[index]=yarray2[i];
			
			
			array2[i] = temp;
			heightarray2[i]=temp2;
			yarray2[i]=temp3;
				
		}
	
		
	}
	
	
		
}