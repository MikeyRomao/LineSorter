package code_mromao_Assignment2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;


public class MyFrame extends JFrame
{
	Screen s;
	Screen2 r;
	JButton j;
	
	public MyFrame()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Set a button on the frame to close the operation
		setSize(800,800); //Setting the size of the screen to 800x800
		setResizable(false);
		setTitle("Assignment 2 - Sorting"); //Set the title of the frame
		init(); 
	}
	
	public void init()
	{
		setLocationRelativeTo(null);
		//Take us to panel 1 where a random triangle is put up
		s=new Screen(); 
		add(s);
		//Create a new button called scramble lines and take us to the 2nd screen for triangle to be scrambeled
		j=new JButton("Scramble Lines");
		add(j,BorderLayout.NORTH);
		j.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				r=new Screen2();
				add(r);
				remove(s);
				remove(j);
				revalidate();
			}
		});
		setVisible(true); //Very important, sets it so the frame is visible and can be seen
	}
	
	public static void main(String[] args)
	{
		new MyFrame(); //Call a new frame
	}
}
