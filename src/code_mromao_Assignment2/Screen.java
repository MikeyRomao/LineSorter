package code_mromao_Assignment2;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import java.util.Random;

public class Screen extends JPanel
{
	static Graphics[] array;
	static int heightarray[],yarray[];
	
	public Screen()
	{
		
	}
	
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		//Create an array of size 256 and type Graphics to hold all the rectangles, so they can be scrambeled
		int x=650,y=100,height=700;//Define the original coordinates of the line
		int size=256;
		
		array=new Graphics[size]; //Create an array to copy the lines into
		heightarray=new int [size];// Create an array
		yarray=new int[size];
		
		Random RandomNumber=new Random(); //Create a random number generator using the java.util.Random class
		for(int i=0;i<size;i++) 
		{
			array[i]=g.create(x,y,1,height);
			heightarray[i]=height;
			yarray[i]=y;
			int randomnumber=RandomNumber.nextInt(60); //Set the range between 0 to 60
			if(randomnumber>=0&&randomnumber<10) //Depending on what the random number is, affects the color of the line
			{
				g.setColor(Color.BLACK);
				g.fillRect(x, y, 1, height);
				//Notice the only non-variable is the width that has a width of 1 pixel (Requirements)	
			}
			
			else if(randomnumber>10&&randomnumber<20)
			{
				g.setColor(Color.GREEN);
				g.fillRect(x, y, 1, height);
			}
			
			else if(randomnumber>20&&randomnumber<30)
			{
				
				g.setColor(Color.ORANGE);
				g.fillRect(x, y, 1, height);
					
			}
			
			else if(randomnumber>30&&randomnumber<40)
			{
				g.setColor(Color.BLUE);
				g.fillRect(x, y, 1, height);
				
			}
			else if(randomnumber>40&&randomnumber<50)
			{
				g.setColor(Color.RED);
				g.fillRect(x, y, 1, height);
			}
			else
			{
				g.setColor(Color.YELLOW);
				g.fillRect(x, y, 1, height);
				
			}
				
			x-=2; // Set the spacing to 2 pixels between each line
			height-=3; //Decrease the height of each line
			y+=3; //To compensate for the decrease in height of the line, shift the line downwards
			
		}
		
	}
		
}
